export const putMessage = (data, cb) => {
    const xhr = new XMLHttpRequest();
    
    xhr.open('PUT', '/putmessage', true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const data = xhr.response;
            
            cb && (cb(JSON.parse(data)));
        }
    };
    xhr.send(JSON.stringify(data));
};

export const putUser = (data, cb) => {
    const xhr = new XMLHttpRequest();
   
    xhr.open('PUT', '/putuser',  true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const data = xhr.response;
           
            cb && (cb(JSON.parse(data)));
        }
    };
    
    xhr.send(JSON.stringify(data));
};

export const putChangeUserState = (data, cb) => {
    const xhr = new XMLHttpRequest();
   
    xhr.open('PUT', '/putchangeuserstate',  true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const data = xhr.response;
           
            cb && (cb(JSON.parse(data)));
        }
    };
    
    xhr.send(JSON.stringify(data));
};

export const putChangeUserName = (data, cb) => {
    const xhr = new XMLHttpRequest();
   
    xhr.open('PUT', '/putchangeusername',  true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const data = xhr.response;
           
            cb && (cb(JSON.parse(data)));
        }
    };
    
    xhr.send(JSON.stringify(data));
};

export const getMessages = (cb) => {
    const xhr = new XMLHttpRequest();

    xhr.open('GET', '/getmessages', true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const data = xhr.response;

            cb && (cb(JSON.parse(data)));
        }
    };
    xhr.send();
};

export const getUsers = (cb) => {
    const xhr = new XMLHttpRequest();

    xhr.open('GET', '/getusers');
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const data = xhr.response;

            cb && (cb(JSON.parse(data)));
        }
    };
    xhr.send();
};
